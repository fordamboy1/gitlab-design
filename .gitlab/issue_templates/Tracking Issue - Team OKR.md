<!--

Title should be: Tracking Issue: UX OKRs for {{Group}} - FY{{YY}}-Q{{quarter number}}
(e.g. “Tracking Issu: UX OKRs for CI/CD - FY22-Q1”)

See example issues: 

https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/22
https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/21

-->

# Overview

We use this issue to track our progress, discuss, and collaborate on the current [UX department OKRs](ADD LINK TO UX DEPARTMENT OKR). This fiscal quarter will run from `_ADD FY DURATION_`. Please leave a comment if you have any questions!

## Issue tasks 

### :o: Opening Tasks

- [ ] Manager: Assign to yourself and your design team.
- [ ] Title the issue `Tracking Issue: UX OKRs for {{Group}} - FY{{YY}}-Q{{quarter number}}`
- [ ] Add an issue comment for your retrospective titled: `:recycle: Retrospective Thread`
- [ ] Fill in the KRs based on the UX Department OKRs.
- [ ] Assign to team members with a specific task or responsibility assigned to them, @ them in the checklist.
- [ ] Share this issue in your Slack channels and with the team members during your next 1:1.

### :x: Closing Tasks

- [ ] Assign back to yourself and remove others.
- [ ] Review any Retrospective items with your team.

---

## Let's Contribute!

The list below only shows KRs where our team input is required. Once you complete a KR, please check your name off the list.

### 1. `_ADD KR TITLE HERE_`

`_ADD KR DESCRIPTION, LINK, AND DUE DATE_`

1. [ ] `@team-member`
1. [ ] `@team-member`
1. [ ] `@team-member`

### 2. `_ADD KR TITLE HERE_`

`_ADD KR DESCRIPTION, LINK, AND DUE DATE_`

1. [ ] `@team-member`
1. [ ] `@team-member`
1. [ ] `@team-member`

<!--

If a KR has its own tracking table, skip the checkboxes above and add the following comment:

❗ This KR has its own tracking table within the above Issue, please use it to track and discuss it.

-->
